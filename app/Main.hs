module Main where

import Helm
import qualified Helm.Engine.SDL as SDL
import Linear.V2 (V2(..))
import Roeship.Engine

main :: IO ()
main = do
  let windowDim = V2 500 500
  engine <- SDL.startupWith $ SDL.defaultConfig
    { SDL.windowIsResizable = False
    , SDL.windowDimensions = windowDim
    , SDL.windowTitle = "Roeship" }
  run engine GameConfig
    { initialFn       = initial windowDim
    , updateFn        = update
    , subscriptionsFn = subscriptions
    , viewFn          = view }
