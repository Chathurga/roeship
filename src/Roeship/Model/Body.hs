{-# LANGUAGE TemplateHaskell #-}

module Roeship.Model.Body
  ( Body
  , HasBody(..)
  , defaultBody
  , move
  , rotate
  , accelerate
  ) where

import Control.Lens
import Linear.V2 (V2(..), angle)
import Linear.Vector ((^+^), (*^))
import Roeship.Angle

data Body = Body
  { _pos :: V2 Double
  , _rotation :: Double
  , _velocity :: V2 Double }
  deriving (Show)

makeClassy ''Body

direction :: HasBody model => Getter model (V2 Double)
direction = to $ views rotation (angle . radians)

defaultBody :: Body
defaultBody = Body { _pos = 0, _rotation = 0, _velocity = 0 }

-- Add the current velocity to a models position
move :: HasBody model => model -> model
move = view velocity >>= \v -> pos %~ (^+^ v)

rotate :: HasBody model => Double -> model -> model
rotate theta = rotation %~ rotateDegrees theta

accelerate :: HasBody model => Double -> model -> model
accelerate magnitude model =
  let delta = magnitude *^ model^.direction
  in  model&velocity %~ (^+^ delta)
