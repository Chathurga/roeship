{-# LANGUAGE TemplateHaskell #-}

module Roeship.Model.Player
  ( PlayerAction(..)
  , Player
  , body
  , actions
  , defaultPlayer
  , start
  , stop
  , applyAction
  , tickPlayer
  ) where

import Control.Lens
import Data.Set (Set)
import qualified Data.Set as Set
import Roeship.Model.Body

data PlayerAction = TurnLeft | TurnRight | Forward | Backward
  deriving (Eq, Ord, Show)

data Player = Player
  { __body :: Body
  , _actions :: Set PlayerAction }
  deriving (Show)

makeLenses ''Player

instance HasBody Player where
  body = _body

defaultPlayer :: Player
defaultPlayer = Player { __body = defaultBody, _actions = Set.empty }

start, stop :: PlayerAction -> Player -> Player
start action = actions %~ Set.insert action
stop action = actions %~ Set.delete action

applyAction :: PlayerAction -> Player -> Player
applyAction action = case action of
  TurnLeft -> rotate (-5)
  TurnRight -> rotate 5
  Forward -> accelerate 0.2
  Backward -> accelerate (-0.2)

tickPlayer :: Player -> Player
tickPlayer player = foldr applyAction (move player) (player^.actions)
