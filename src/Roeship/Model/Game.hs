{-# LANGUAGE TemplateHaskell #-}

module Roeship.Model.Game
  ( Game
  , GameState(..)
  , defaultGame
  , windowDim
  , tickCount
  , mode
  , player
  , startGame
  , tick
  , score
  , boundryDistance
  ) where

import Control.Lens
import Linear.Vector ((^/))
import Linear.V2 (V2(..))
import Roeship.Model.Body
import Roeship.Model.Player

data GameState = PreGame | InGame deriving (Eq, Show)

data Game = Game
  { _windowDim :: V2 Double
  , _tickCount :: Integer
  , _mode :: GameState
  , _player :: Player }
  deriving (Show)

makeLenses ''Game

score :: Getter Game Integer
score = to $ views tickCount (`div` 60)

defaultGame :: Game
defaultGame = Game
  { _windowDim = 0
  , _tickCount = 0
  , _mode = PreGame
  , _player = defaultPlayer }

startGame :: V2 Double -> Game
startGame _windowDim =
  defaultGame &
    (windowDim .~ _windowDim) .
    (player.pos .~ (_windowDim ^/ 2))

tick :: Game -> Game
tick = (tickCount %~ (+ 1)) . (player %~ tickPlayer)

boundryDistance :: V2 Double -> V2 Double -> Double
boundryDistance (V2 x y) (V2 w h) = minimum [x, w - x, y, h - y]
