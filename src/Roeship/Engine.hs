module Roeship.Engine
  ( initial
  , update
  , subscriptions
  , view
  ) where

import Control.Lens ((.~), (%~), (&), (^.))
import Helm
import qualified Helm.Cmd as Cmd
import qualified Helm.Keyboard as Keyboard
import Helm.Engine.SDL (SDLEngine)
import qualified Helm.Sub as Sub
import qualified Helm.Time as Time
import Linear.V2 (V2(..))
import Roeship.Model.Game
import Roeship.Model.Player
import Roeship.View.Game

data GameAction
  = Idle
  | Tick
  | Start PlayerAction
  | Stop PlayerAction
  | StartGame

initial :: V2 Int -> (Game, Cmd SDLEngine GameAction)
initial windowDim =
  let wh = fmap fromIntegral windowDim
  in  (startGame wh, Cmd.none)

updateInGame :: Game -> GameAction -> (Game, Cmd SDLEngine GameAction)
updateInGame game gameAction = case gameAction of
  Tick         -> (tick game, Cmd.none)
  Start action -> (game&player %~ start action, Cmd.none)
  Stop action  -> (game&player %~ stop action, Cmd.none)
  _            -> (game, Cmd.none)

updatePreGame game gameAction = case gameAction of
  StartGame -> (game&mode .~ InGame, Cmd.none)
  _         -> (game, Cmd.none)

update :: Game -> GameAction -> (Game, Cmd SDLEngine GameAction)
update game = case game^.mode of
  InGame  -> updateInGame game
  PreGame -> updatePreGame game

subscriptions :: Sub SDLEngine GameAction
subscriptions = Sub.batch
  [ Keyboard.downs $ \key -> (case key of
      Keyboard.LeftKey  -> Start TurnLeft
      Keyboard.RightKey -> Start TurnRight
      Keyboard.UpKey    -> Start Forward
      Keyboard.DownKey  -> Start Backward
      _                 -> Idle)
  , Keyboard.ups $ \key -> (case key of
      Keyboard.LeftKey  -> Stop TurnLeft
      Keyboard.RightKey -> Stop TurnRight
      Keyboard.UpKey    -> Stop Forward
      Keyboard.DownKey  -> Stop Backward
      _                 -> Idle)
  , Keyboard.presses $ \key -> (case key of
      Keyboard.SpaceKey -> StartGame
      _                 -> Idle)
  , Time.fps 60 (const Tick) ]

view :: Game -> Graphics SDLEngine
view game = Graphics2D $ gameView game
