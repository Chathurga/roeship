module Roeship.View.Game
  ( gameView
  ) where

import Control.Lens
import Helm.Color
import Helm.Graphics2D
import Helm.Graphics2D.Text
import Linear.Vector ((^+^), (^/))
import Linear.V2 (V2(..), _x)
import Roeship.Model.Body (pos)
import Roeship.Model.Game
import Roeship.View.Player

boundryColor :: V2 Double -> V2 Double -> Color
boundryColor pos wh
  | distance > zone = (rgb 0 1 0)
  | distance < 0    = (rgb 1 0 0)
  | otherwise       = (rgb (1 - distance / zone) (distance / zone) 0)
  where
    distance :: Double
    distance = boundryDistance pos wh
    zone :: Double
    zone = 90

boundryView :: V2 Double -> V2 Double -> Form e
boundryView pos wh@(V2 w h) =
  let color = boundryColor pos wh
      shapes =
        [ (filled color $ rect (V2 w 6), V2 (w/2) 3, V2 0 (h-6))
        , (filled color $ rect (V2 6 h), V2 3 (h/2), V2 (w-6) 0) ]
  in  group $ shapes >>= \(shape, pos, offset) ->
        [move pos shape, move (pos ^+^ offset) shape]

scoreView :: Game -> Form e
scoreView game =
  let scorePos = V2 (game^.windowDim._x / 2) 50
  in  move scorePos $ text $ color (rgb 1 1 1) $
      typeface "monospace" $ toText $ lpad 6 '0' $ show $ game^.score
  where
    lpad :: Int -> Char -> [Char] -> [Char]
    lpad m c xs = let ys = take m xs in replicate (m - length ys) c ++ ys

menuView :: Game -> Form e
menuView game =
  move (game^.windowDim ^/ 2) $ text $ color (rgb 1 1 1) $
  typeface "monospace" $ toText $ "Press space to start"

gameView :: Game -> Collage e
gameView game =
  collage $ case game^.mode of
    InGame ->
      [ playerView (game^.player)
      , boundryView (game^.player.pos) (game^.windowDim)
      , scoreView game ]
    PreGame ->
      [ boundryView (game^.windowDim ^/ 2) (game^.windowDim)
      , menuView game ]
