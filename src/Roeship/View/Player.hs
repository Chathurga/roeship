module Roeship.View.Player
  ( playerView
  ) where

import Control.Lens
import Helm.Color
import Helm.Graphics2D
import Helm.Graphics2D.Text
import Roeship.Angle
import Roeship.Model.Body (rotation, pos)
import Roeship.Model.Player

playerView :: Player -> Form e
playerView player =
  rotate (radians (player^.rotation)) $ move (player^.pos) $ text $
  color (rgb 1 0 0) (toText "Roe>")
