module Roeship.Angle
  ( radians
  , rotateDegrees
  ) where

radians :: Double -> Double
radians deg = (deg * pi) / 180

rotateDegrees :: Double -> Double -> Double
rotateDegrees theta angle
  | angle' < 0   = angle' + 360
  | angle' > 360 = angle' - 360
  | otherwise    = angle'
  where angle' = angle + theta
